
#include <WiFi.h>
#include <HTTPClient.h>
#include <Arduino_JSON.h>

const char* ssid = "Ngumb-H64";
const char* password = "DCollins@123";

//Your Domain name with URL path or IP address with path
const char* serverName = "http://192.168.100.19:8000/status";


unsigned long lastTime = 0;
unsigned long timerDelay = 50;

int pressure_sensor_pin_1 =6;
int pressure_sensor_pin_2 =7;
int pressure_sensor_pin_3 =8;
int pressure_sensor_pin_4 =9;


int LED_pin_1 =10;
int LED_pin_2 =11;
int LED_pin_3 =12;
int LED_pin_4 =13;


int Relay_Pin_1 = 14;
int Relay_Pin_2 = 15;
int Relay_Pin_3 = 16;
int Relay_Pin_4 = 17;



String pressure_sensor_var_1 = "OFF";
String pressure_sensor_var_2 = "OFF";
String pressure_sensor_var_3 = "OFF";
String pressure_sensor_var_4= "OFF";


String LED_var_1 = "OFF";
String LED_var_2 = "OFF";
String LED_var_3 = "OFF";
String LED_var_4 = "OFF";

String Relay_var_1 ="OFF";
String Relay_var_2 ="OFF";
String Relay_var_3 ="OFF";
String Relay_var_4 ="OFF";



String server_response;
float sensorReadingsArr[3];
String server_post ="http://192.168.100.19:8000/sensor_post_2";











void setup() {
  pinMode(2, OUTPUT);
  Serial.begin(115200);

  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
 
}


void loop(){
    post_request();
  
}







void get_request() {
  //Send an HTTP POST request every 10 minutes
  if ((millis() - lastTime) > timerDelay) {
    //Check WiFi connection status
    if(WiFi.status()== WL_CONNECTED){
              
      server_response = httpGETRequest(serverName);
      Serial.print("sensor readings:");
      Serial.println(sizeof(server_response.toInt()));

      JSONVar myObject = JSON.parse(server_response);
  
      // JSON.typeof(jsonVar) can be used to get the type of the var
      if (JSON.typeof(myObject) == "undefined") {
        Serial.println("Parsing input failed!");
        return;
      }
    
      Serial.print("JSON object = ");
      Serial.println(myObject[0]);
      String obj = myObject[0];
      Serial.print("obj from server:");
      Serial.println(obj);

      if(obj == "1"){
        Serial.println("ON");
        digitalWrite(2, HIGH);
      }else{
           digitalWrite(2, LOW);
          Serial.println("OFF");
      }

            
    }
    else {
      Serial.println("WiFi Disconnected");
    }
    lastTime = millis();
  }
}











void functionality() {
//      update_status();


  // put your main code here, to run repeatedly:

    int sensorValue_1 = digitalRead(pressure_sensor_pin_1);
    int sensorValue_2 = digitalRead(pressure_sensor_pin_2);
    int sensorValue_3 = digitalRead(pressure_sensor_pin_3);
    int sensorValue_4 = digitalRead(pressure_sensor_pin_4);


    
  if (sensorValue_1 == HIGH) {
    // Do something if the pin is HIGH
      pressure_sensor_var_1="ON"; 
      digitalWrite(LED_pin_1, HIGH);
      LED_var_1 = "ON";
      digitalWrite(Relay_Pin_1, LOW);
      Relay_var_1="ON";

  } else if(sensorValue_2 == HIGH){
    pressure_sensor_var_2="ON";
    digitalWrite(LED_pin_2, HIGH);
    LED_var_2 = "ON";
    digitalWrite(Relay_Pin_2, LOW);
    Relay_var_2="ON";
    
  }else if(sensorValue_3 == HIGH){
    pressure_sensor_var_3="ON";
    digitalWrite(LED_pin_3, HIGH);
    LED_var_3 = "ON";
    digitalWrite(Relay_Pin_3, LOW);
    Relay_var_3="ON";
    
  }else if(sensorValue_4 == HIGH){
    pressure_sensor_var_4="ON";
    digitalWrite(LED_pin_4, HIGH);
    LED_var_4 = "ON";
    digitalWrite(Relay_Pin_4, LOW);
    Relay_var_4="ON";
    
  }else {
    //else everything is OFF
    // Do something if the pin is LOW
    digitalWrite(LED_pin_1, LOW);
    digitalWrite(LED_pin_2, LOW);
    digitalWrite(LED_pin_3, LOW);
    digitalWrite(LED_pin_4, LOW);

    digitalWrite(Relay_Pin_1, HIGH);
    digitalWrite(Relay_Pin_2, HIGH);
    digitalWrite(Relay_Pin_3, HIGH);
    digitalWrite(Relay_Pin_4, HIGH);

    pressure_sensor_var_1 = "OFF";
    pressure_sensor_var_2 = "OFF";
    pressure_sensor_var_3 = "OFF";
    pressure_sensor_var_4 = "OFF";

    LED_var_2 = "OFF";
    LED_var_3 = "OFF";
    LED_var_4 = "OFF";

    Relay_var_1 ="OFF";
    Relay_var_2 ="OFF";
    Relay_var_3 ="OFF";
    Relay_var_4 ="OFF";



  
  }


}





String httpGETRequest(const char* serverName) {
  WiFiClient client;
  HTTPClient http;
    
  // Your Domain name with URL path or IP address with path
  http.begin(client, serverName);
  
  
  // Send HTTP POST request
  int httpResponseCode = http.GET();
  
  String payload = "{}"; 
  
  if (httpResponseCode>0) {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    payload = http.getString();
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  // Free resources
  http.end();

  return payload;
}





void post_request() {
  //Send an HTTP POST request every 10 minutes
  delay(1000);
  if ((millis() - lastTime) > timerDelay) {
    //Check WiFi connection status
    if(WiFi.status()== WL_CONNECTED){
      WiFiClient client;
      HTTPClient http;
    
      // Your Domain name with URL path or IP address with path
      http.begin(client, server_post);
      

      http.addHeader("Content-Type", "application/json");
        String Data = "{\"led_1\":\"" + LED_var_1 +
                      "\",\"led_2\":\"" + LED_var_2 + 
                      "\",\"led_3\":\"" + LED_var_3 +
                      "\",\"led_4\":\"" + LED_var_4 +
                  
                     "\",\"relay_1\":\"" + Relay_var_1 + 
                      "\",\"relay_2\":\"" + Relay_var_2 + 
                      "\",\"relay_3\":\"" + Relay_var_3 +
                      "\",\"relay_4\":\"" + Relay_var_4 +

                     "\",\"pressure_sensor_1\":\"" + pressure_sensor_var_1 + 
                      "\",\"pressure_sensor_2\":\"" + pressure_sensor_var_2 + 
                       "\",\"pressure_sensor_3\":\"" + pressure_sensor_var_3 + 
                        "\",\"pressure_sensor_4\":\"" + pressure_sensor_var_4 + 
                        
                       "\"}";

      int httpResponseCode = http.POST(Data);


      // If you need an HTTP request with a content type: text/plain
      //http.addHeader("Content-Type", "text/plain");
      //int httpResponseCode = http.POST("Hello, World!");
     
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
      Serial.print(Data);
        
      // Free resources
      http.end();
    }
    else {
      Serial.println("WiFi Disconnected");
    }
    lastTime = millis();
  }
}
