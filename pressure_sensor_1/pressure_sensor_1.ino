
#include <WiFi.h>
#include <HTTPClient.h>
#include <Arduino_JSON.h>

const char* ssid = "Ngumb-H64";
const char* password = "DCollins@123";

//Your Domain name with URL path or IP address with path
const char* serverName = "http://192.168.100.19:8000/status";


unsigned long lastTime = 0;

unsigned long timerDelay = 50;

String server_response;
float sensorReadingsArr[3];
String server_post ="http://192.168.100.19:8000/sensor_post";

String pressure_sensor_var = "OFF";
String LED_var = "OFF";
String Relay_var ="OFF";



int pressure_sensor_pin =6;
int LED_pin =5;
int Relay_Pin = 7;







void setup() {
  pinMode(2, OUTPUT);
  Serial.begin(115200);

  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
 
}


void loop(){
//  functionality();
//  update_status();
    post_request();
  
}





 //UPDATE STATUS
void update_status() {
 
 if(WiFi.status()== WL_CONNECTED){
 
   HTTPClient http;   
 
   http.begin("http://192.168.100.19:8000/update-status-project-1/"+pressure_sensor_var+LED_var+Relay_var);
   http.addHeader("Content-Type", "text/plain");            
 
   int httpResponseCode = http.PUT("PUT sent from ESP32");   
 
   if(httpResponseCode>0){
 
    String response = http.getString();   
 
    Serial.println(httpResponseCode);
    Serial.println(response);          
 
   }else{
 
    Serial.print("Error on sending PUT Request: ");
    Serial.println(httpResponseCode);
 
   }
 
   http.end();
 
 }else{
    Serial.println("Error in WiFi connection");
 }
 
  delay(1000);
}







void get_request() {
  //Send an HTTP POST request every 10 minutes
  if ((millis() - lastTime) > timerDelay) {
    //Check WiFi connection status
    if(WiFi.status()== WL_CONNECTED){
              
      server_response = httpGETRequest(serverName);
      Serial.print("sensor readings:");
      Serial.println(sizeof(server_response.toInt()));

      JSONVar myObject = JSON.parse(server_response);
  
      // JSON.typeof(jsonVar) can be used to get the type of the var
      if (JSON.typeof(myObject) == "undefined") {
        Serial.println("Parsing input failed!");
        return;
      }
    
      Serial.print("JSON object = ");
      Serial.println(myObject[0]);
      String obj = myObject[0];
      Serial.print("obj from server:");
      Serial.println(obj);

      if(obj == "1"){
        Serial.println("ON");
        digitalWrite(2, HIGH);
      }else{
           digitalWrite(2, LOW);
          Serial.println("OFF");
      }

            
    }
    else {
      Serial.println("WiFi Disconnected");
    }
    lastTime = millis();
  }
}











void functionality() {
//      update_status();


  // put your main code here, to run repeatedly:
  digitalRead(pressure_sensor_pin);

    int sensorValue = digitalRead(pressure_sensor_pin);
    
    pressure_sensor_var="ON";
  if (sensorValue == HIGH) {
    // Do something if the pin is HIGH
    digitalWrite(LED_pin, HIGH);
    
    LED_var = "ON";
    
    digitalWrite(Relay_Pin, LOW);
    
    Relay_var="ON";

  } else {
    //Else everything is OFF
    // Do something if the pin is LOW
    digitalWrite(LED_pin, LOW);
    digitalWrite(Relay_Pin, HIGH);

    pressure_sensor_var = "OFF";
    LED_var = "OFF";
    Relay_var ="OFF";



   // update_status();
  
  }


}





String httpGETRequest(const char* serverName) {
  WiFiClient client;
  HTTPClient http;
    
  // Your Domain name with URL path or IP address with path
  http.begin(client, serverName);
  
  
  // Send HTTP POST request
  int httpResponseCode = http.GET();
  
  String payload = "{}"; 
  
  if (httpResponseCode>0) {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    payload = http.getString();
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  // Free resources
  http.end();

  return payload;
}





void post_request() {
  //Send an HTTP POST request every 10 minutes
  delay(1000);
  if ((millis() - lastTime) > timerDelay) {
    //Check WiFi connection status
    if(WiFi.status()== WL_CONNECTED){
      WiFiClient client;
      HTTPClient http;
    
      // Your Domain name with URL path or IP address with path
      http.begin(client, server_post);
      

      http.addHeader("Content-Type", "application/json");
        String Data = "{\"led\":\"" + LED_var +
                     "\",\"relay\":\"" + Relay_var + 
                     "\",\"pressure_sensor\":\"" + pressure_sensor_var + 
                       "\"}";

      int httpResponseCode = http.POST(Data);


      // If you need an HTTP request with a content type: text/plain
      //http.addHeader("Content-Type", "text/plain");
      //int httpResponseCode = http.POST("Hello, World!");
     
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
        
      // Free resources
      http.end();
    }
    else {
      Serial.println("WiFi Disconnected");
    }
    lastTime = millis();
  }
}
